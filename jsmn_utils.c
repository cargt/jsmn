/*
 * jsmn_utils.c
 *
 *  Created on: Nov 11, 2017
 *      Author: Graham Taylor-Jones
 *      Company: Cargt Inc
 */
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "errno.h"

#include "jsmn.h"
#include "jsmn_utils.h"

int jsmn_utils_jsoneq(const char *json, const jsmntok_t *tok, const char *s) {
	if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
			strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
		return 0;
	}
	return -1;
}

int jsmn_utils_get_uint32(const char *json, const jsmntok_t *token, uint32_t *result, bool useJsonStrict)
{
	/* Logic:
	 * if strict == true, only accept primitive,
	 * only accept string if use strict is false
	 * reject all others
	 */
	if ((JSMN_PRIMITIVE != token->type)
		&& ((JSMN_STRING != token->type) || (useJsonStrict)))
	{
	return -1;
	}

	char *tail;
	unsigned long int number = strtoul(json + (token->start), &tail, 10);
	int errVal = errno;
	if ((EINVAL == errVal) || (ERANGE == errVal)) // Not a number or in range
		return -1;
	if (tail != json + (token->end)) // didn't consume the whole string
	{
		return -1;
	}

	*result = number;
	return 1;
}

int jsmn_utils_get_int8(const char *json, const jsmntok_t *token, int8_t *result, bool useJsonStrict)
{
	/* Logic:
	 * if strict == true, only accept primitive,
	 * only accept string if use strict is false
	 * reject all others
	 */
	if ((JSMN_PRIMITIVE != token->type)
		&& ((JSMN_STRING != token->type) || (useJsonStrict)))
	{
	return -1;
	}

	char *tail;
	long int number = strtol(json + (token->start), &tail, 10);
	int errVal = errno;
	if ((EINVAL == errVal) || (ERANGE == errVal)) // Not a number or in range
		return -1;
	if (tail != json + (token->end)) // didn't consume the whole string
	{
		return -1;
	}

	*result = (int8_t)number;
	return 1;
}

int jsmn_utils_get_float(const char *json, const jsmntok_t *token, float *result, bool useJsonStrict)
{
	/* Logic:
	 * if strict == true, only accept primitive,
	 * only accept string if use strict is false
	 * reject all others
	 */
	if ((JSMN_PRIMITIVE != token->type)
		&& ((JSMN_STRING != token->type) || (useJsonStrict)))
	{
	return -1;
	}

	char *tail;
	float number = strtof(json + (token->start), &tail);
	int errVal = errno;
	if ((EINVAL == errVal) || (ERANGE == errVal)) // Not a number or in range
		return -1;
	if (tail != json + (token->end)) // didn't consume the whole string
	{
		return -1;
	}

	*result = number;
	return 1;
}

#if 0 // not implemented yet
int jsmn_utils_get_int32(const char *json, const jsmntok_t *token, int32_t *result, bool useJsonStrict)
{
	return -1;
}

int jsmn_utils_get_boolean(const char *json, const jsmntok_t *token, bool *result, bool useJsonStrict)
{
	return -1;
}
#endif

int jsmn_utils_get_string(const char *json, const jsmntok_t *token, char *result, uint16_t bufLen, bool allowEmpty)
{
	/* Logic:
	 * Must be a string primitive
	 * reject all others
	 */
	if (JSMN_STRING != token->type) return -1;
	int len = token->end - token->start;
	if ((false == allowEmpty) && (len <= 0)) return -1;
	if ((true == allowEmpty) && (len < 0)) return -1;

	if (bufLen > len) // leave room of extra null char
	{
		memcpy(result, json + (token->start), len);
		*(result + len) = '\0'; // insert null char
		return len;
	}
	else
	{
		return -1;
	}
}

