/*
 * jsmn_utils.h
 *
 *  Created on: Nov 11, 2017
 *      Author: Graham Taylor-Jones
 *      Company: Cargt Inc.
 */

#ifndef JSMN_JSMN_UTILS_H_
#define JSMN_UTILS_H_

#include "jsmn.h"

/**
 * @brief Check if test string matches input json pointed at by jsmn token.
 * @param json json string used by jsmn parser
 * @param tok token contains offset values for json string to allow selection of data
 * @param s - test string to compare json to
 * @return 0 on match, -1 otherwise.
 */
int jsmn_utils_jsoneq(const char *json, const jsmntok_t *tok, const char *s);

/**
 * @brief Get and convert value from json as identified by a jsmn token struct
 * @param json json string used by jsmn parser
 * @param tok token contains offset values for json string to allow selection of data
 * @param *result - output the correctly parsed value type
 * @param useJsonStrict - if true then strict json parsing is applied.
 * 			strict - means boolean is true/false (primitive without " marks)
 * 			if false - allow strings to be converted to primitives and bool = 0/1 or true/false
 * @return 0 on match, -1 otherwise.
 */
int jsmn_utils_get_uint32(const char *json, const jsmntok_t *token, uint32_t *result, bool useJsonStrict);
int jsmn_utils_get_int8(const char *json, const jsmntok_t *token, int8_t *result, bool useJsonStrict);
int jsmn_utils_get_float(const char *json, const jsmntok_t *token, float *result, bool useJsonStrict);

#if 0 // not implemented yet
/**
 * @brief Get and convert value from json as identified by a jsmn token struct
 * @param json json string used by jsmn parser
 * @param tok token contains offset values for json string to allow selection of data
 * @param *result - output the correctly parsed value type
 * @param useJsonStrict - if true then strict json parsing is applied.
 * 			strict - means boolean is true/false (primitive without " marks)
 * 			if false - allow strings to be converted to primitives and bool = 0/1 or true/false
 * @return 0 on match, -1 otherwise.
 */
int jsmn_utils_get_int32(const char *json, const jsmntok_t *token, int32_t *result, bool useJsonStrict);

/**
 * @brief Get and convert value from json as identified by a jsmn token struct
 * @param json json string used by jsmn parser
 * @param tok token contains offset values for json string to allow selection of data
 * @param *result - output the correctly parsed value type
 * @param useJsonStrict - if true then strict json parsing is applied.
 * 			strict - means boolean is true/false (primitive without " marks)
 * 			if false - allow strings to be converted to primitives and bool = 0/1 or true/false
 * @return 0 on match, -1 otherwise.
 */
int jsmn_utils_get_boolean(const char *json, const jsmntok_t *token, bool *result, bool useJsonStrict);
#endif

/**
 * @brief Get and convert value from json as identified by a jsmn token struct
 * @param json json string used by jsmn parser
 * @param tok token contains offset values for json string to allow selection of data
 * @param *result - output the correctly parsed value type
 * @param bufLen - maximum length of result buffer - needs to have space for NULL character
 * @return len of string (equiv to strlen() not including null char), -1 otherwise.
 * 		NULL '\0' is added after end valid of string
 */
int jsmn_utils_get_string(const char *json, const jsmntok_t *token, char *result, uint16_t resultBufLen, bool allowEmpty);

#endif /* JSMN_UTILS_H_ */
